pub use sdl2::{
	pixels::Color,
	rect::Rect,
};

pub type Canvas = sdl2::render::Canvas<sdl2::video::Window>;

pub enum Side {
	Left,
	Right,
	Top,
	Bottom,
}

pub mod widget;

#[macro_export]
macro_rules! horizontal {
	($($x:expr),+) => (
        $crate::widget::Horizontal::from_vec(
			<[_]>::into_vec(Box::new([$(Box::new($x)),*]))
		)
	);
	($($x:expr,)+) => (
		$crate::horizontal![$($x),*]
	);
}
// #[macro_export]
// macro_rules! vertical {
// 	($($x:expr),+) => (
//         $crate::widget::Vertical::from_vec(
// 			<[_]>::into_vec(Box::new([$(Box::new($x)),*]))
// 		)
// 	);
// 	($($x:expr,)+) => (
// 		$crate::vertical![$($x),*]
// 	);
// }

#[macro_export]
macro_rules! pickable {
	() => {
		fn pick(&self, size: (u32, u32), point: (u32, u32)) -> Option<$crate::widget::Picked> {
			Some($crate::widget::Picked {
				widget: self,
				size: size,
				point: point,
			})
		}
		fn pick_mut(&mut self, size: (u32, u32), point: (u32, u32)) -> Option<$crate::widget::PickedMut> {
			Some($crate::widget::PickedMut {
				widget: self,
				size: size,
				point: point,
			})
		}	
	};
}

pub struct App<T: widget::Widget> {
	root_widget: T,
	dimensions: (usize, usize),
	fps: usize,
}

impl<T: widget::Widget> App<T> {
	pub fn new(root_widget: T) -> Self {
		Self {
			root_widget,
			dimensions: (800, 600),
			fps: 60,
		}
	}
	pub fn dimensions(mut self, width: usize, height: usize) -> Self {
		self.dimensions = (width, height);
		self
	}
	pub fn fps(mut self, fps: usize) -> Self {
		assert_ne!(fps, 0);
		self.fps = fps;
		self
	}
	pub fn run(mut self) {
		use sdl2::event::Event;
		use sdl2::keyboard::Keycode;
		use std::time::Duration;
	
		let sdl_context = sdl2::init().unwrap();
		let video_subsystem = sdl_context.video().unwrap();

		let viewport = Rect::new(
			0, 0, self.dimensions.0 as u32, self.dimensions.1 as u32
		);
	
		let window = video_subsystem
			.window("gui", viewport.width(), viewport.height())
			.position_centered()
			.resizable()
			.build()
			.unwrap();
	
		let mut canvas = window.into_canvas().build().unwrap();
	
		let mut event_pump = sdl_context.event_pump().unwrap();
		'running: loop {
			for event in event_pump.poll_iter() {
				match event {
					Event::Quit { .. }
					| Event::KeyDown {
						keycode: Some(Keycode::Escape),
						..
					} => break 'running,
					Event::MouseButtonDown{x, y, ..} => {
						if let Some(widget::PickedMut {widget, size, point}) = self.root_widget.pick_mut(
							(viewport.width(), viewport.height()),
							(x as u32, y as u32),
						) {
							widget.event_clicked(size, point);
						}
					}
					_ => {}
				}
			}
			let viewport = canvas.viewport();
			self.root_widget.render(&mut canvas, viewport, Color::BLACK).unwrap();
			canvas.present();
	
			::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / self.fps as u32));
		}
	}
}
