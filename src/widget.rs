use super::*;

pub struct Picked<'a> {
	pub widget: &'a dyn Widget,
	pub size: (u32, u32),
	pub point: (u32, u32),
}

pub struct PickedMut<'a> {
	pub widget: &'a mut dyn Widget,
	pub size: (u32, u32),
	pub point: (u32, u32),
}

pub trait Widget {
	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) -> Result<(), String>;
	fn pick(&self, _size: (u32, u32), _point: (u32, u32)) -> Option<Picked> {None}
	fn pick_mut(&mut self, _size: (u32, u32), _point: (u32, u32)) -> Option<PickedMut> {None}
	fn event_clicked(&mut self, _size: (u32, u32), _point: (u32, u32)) {}


	fn margin(self, horizontal: u32, vertical: u32) -> Margin<Self>
		where Self: Sized
	{
		Margin::new(self, horizontal, vertical)
	}
	fn background(self, color: Color) -> Background<Self>
		where Self: Sized
	{
		Background::new(self, color)
	}
}


pub struct Empty;
impl Widget for Empty {
	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) -> Result<(), String> {
		canvas.set_draw_color(bg_color);
		canvas.fill_rect(clip)
	}
}

impl Widget for Color {
	fn render(&self, canvas: &mut Canvas, clip: Rect, _: Color) -> Result<(), String> {
		canvas.set_draw_color(*self);
		canvas.fill_rect(clip)
	}
}

pub struct Horizontal {
	widgets: Vec<Box<dyn Widget>>,
}
impl Horizontal {
	pub fn from_vec(widgets: Vec<Box<dyn Widget>>) -> Self {
		assert_ne!(widgets.len(), 0);
		Self{ widgets }
	}
}
impl Widget for Horizontal {
	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) -> Result<(), String> {
		assert_ne!(self.widgets.len(), 0);
		let divided = clip.width() / self.widgets.len() as u32;
		let remain  = clip.width() % self.widgets.len() as u32;
		let mut x = 0;
		for (i, widget) in self.widgets.iter().enumerate() {
			let width = divided + (if (i as u32) < remain {1} else {0});
			widget.render(canvas, Rect::new(
				clip.x() + x as i32,
				clip.y(),
				width,
				clip.height(),
			), bg_color)?;
			x += width;
		}
		Ok(())
	}
	fn pick(&self, (width, height): (u32, u32), (x, y): (u32, u32)) -> Option<Picked> {
		assert!(x < width && y < height);
		assert_ne!(self.widgets.len(), 0);
		let divided = width / self.widgets.len() as u32;
		let remain  = width % self.widgets.len() as u32;
		let mut mx = 0;
		for (i, widget) in self.widgets.iter().enumerate() {
			let width = divided + (if (i as u32) < remain {1} else {0});
			if (mx .. mx + width).contains(&x) {
				return widget.pick((width, height), (mx, y))
			}
			mx += width;
		}
		None
	}
	fn pick_mut(&mut self, (width, height): (u32, u32), (x, y): (u32, u32)) -> Option<PickedMut> {
		assert!(x < width && y < height);
		assert_ne!(self.widgets.len(), 0);
		let divided = width / self.widgets.len() as u32;
		let remain  = width % self.widgets.len() as u32;
		let mut mx = 0;
		for (i, widget) in self.widgets.iter_mut().enumerate() {
			let width = divided + (if (i as u32) < remain {1} else {0});
			if (mx .. mx + width).contains(&x) {
				return widget.pick_mut((width, height), (x - mx, y))
			}
			mx += width;
		}
		None
	}
}

// pub struct Vertical {
// 	widgets: Vec<Box<dyn Widget>>,
// }
// impl Vertical {
// 	pub fn new<T: Widget + 'static>(first: T) -> Self {
// 		Self{
// 			widgets: vec![Box::new(first)],
// 		}
// 	}
// 	pub fn from_vec(widgets: Vec<Box<dyn Widget>>) -> Self {
// 		assert_ne!(widgets.len(), 0);
// 		Self{
// 			widgets,
// 		}
// 	}
// 	pub fn push<T: Widget + 'static>(&mut self, elem: T) {
// 		self.widgets.push(Box::new(elem))
// 	}
// }
// impl Widget for Vertical {
// 	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) {
// 		assert_ne!(self.widgets.len(), 0);
// 		let divided = clip.height() / self.widgets.len() as u32;
// 		let remain  = clip.height() % self.widgets.len() as u32;
// 		let mut y = 0;
// 		for (i, widget) in self.widgets.iter().enumerate() {
// 			let height = divided + (if (i as u32) < remain {1} else {0});
// 			widget.render(canvas, Rect::new(
// 				clip.x(),
// 				clip.y() + y as i32,
// 				clip.width(),
// 				height,
// 			), bg_color);
// 			y += height;
// 		}
// 	}
// 	fn pick(&self, _width: u32, height: u32, _x: u32, y: u32) -> Option<&dyn Widget> {
// 		let divided = height / self.widgets.len() as u32;
// 		let remain  = height % self.widgets.len() as u32;
// 		let mut my = 0;
// 		for (i, widget) in self.widgets.iter().enumerate() {
// 			let height = divided + (if (i as u32) < remain {1} else {0});
// 			if (my .. my + height).contains(&y) {
// 				return Some(widget.as_ref())
// 			}
// 			my += height;
// 		}
// 		None
// 	}
// }

pub struct Margin<T: Widget> {
	widget: T,
	horizontal: u32,
	vertical: u32,
}
impl<T: Widget> Margin<T> {
	pub fn new(widget: T, horizontal: u32, vertical: u32) -> Self {
		Self{
			widget,
			horizontal,
			vertical,
		}
	}
	fn inner_rect(&self, clip: Rect) -> Option<Rect> {
		match (
			clip.width().checked_sub(self.horizontal * 2),
			clip.height().checked_sub(self.vertical * 2),
		) {
			(Some(width), Some(height)) if width > 0 && height > 0 => Some(
				Rect::new(
					clip.x() + self.horizontal as i32,
					clip.y() + self.vertical as i32,
					width,
					height
				)
			),
			_ => None,
		}
	}
}
impl<T: Widget> Widget for Margin<T> {
	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) -> Result<(), String> {
		canvas.set_draw_color(bg_color);
		canvas.fill_rect(clip)?;
		if let Some(inner_rect) = self.inner_rect(clip) {
			self.widget.render(canvas, inner_rect, bg_color)?;
		}
		Ok(())
	}
	fn pick(&self, (width, height): (u32, u32), (x, y): (u32, u32)) -> Option<Picked> {
		assert!(x < width && y < height);
		match (
			width .checked_sub(self.horizontal * 2),
			height.checked_sub(self.vertical   * 2),
			x     .checked_sub(self.horizontal    ),
			y     .checked_sub(self.vertical      ),
		) {
			(
				Some(width),
				Some(height),
				Some(x),
				Some(y)
			) if x < width && y < height => self.widget.pick((width, height), (x, y)),
			_ => None,
		}
	}
	fn pick_mut(&mut self, (width, height): (u32, u32), (x, y): (u32, u32)) -> Option<PickedMut> {
		assert!(x < width && y < height);
		match (
			width .checked_sub(self.horizontal * 2),
			height.checked_sub(self.vertical   * 2),
			x     .checked_sub(self.horizontal    ),
			y     .checked_sub(self.vertical      ),
		) {
			(
				Some(width),
				Some(height),
				Some(x),
				Some(y)
			) if x < width && y < height => self.widget.pick_mut((width, height), (x, y)),
			_ => None,
		}
	}
}

pub struct Background<T: Widget> {
	widget: T,
	color: Color,
}
impl<T: Widget> Background<T> {
	fn new(widget: T, color: Color) -> Self {
		Self{ widget, color }
	}
}
impl<T: Widget> Widget for Background<T> {
	fn render(&self, canvas: &mut Canvas, clip: Rect, _: Color) -> Result<(), String> {
		self.widget.render(canvas, clip, self.color)
	}
	fn pick(&self, size: (u32, u32), point: (u32, u32)) -> Option<Picked> {
		self.widget.pick(size, point)
	}
	fn pick_mut(&mut self, size: (u32, u32), point: (u32, u32)) -> Option<PickedMut> {
		self.widget.pick_mut(size, point)
	}
}

// pub struct SplitAbs<T: Widget, U: Widget> {
// 	inner: T,
// 	outer: U,
// 	side: Side,
// 	size: u32,
// }
// impl<T: Widget, U: Widget> SplitAbs<T, U> {
// 	fn new(inner: T, outer: U, side: Side, size: u32) -> Self {
// 		Self{ inner, outer, side, size }
// 	}
// }
// impl<T: Widget, U: Widget> Widget for SplitAbs<T, U> {
// 	fn render(&self, canvas: &mut Canvas, clip: Rect, bg_color: Color) -> Result<(), String> {
// 		match self.side {
// 			Side::Left => {
// 				if clip.width() > self.size {

// 				}
// 				match clip.width() {
// 					x if x > self.size => {

// 					}
// 					x if x 
// 				}
// 			}
// 			Side::Right => {

// 			}
// 			Side::Top => {

// 			}
// 			Side::Bottom => {

// 			}
// 		}
// 		Ok(())
// 	}
// 	fn pick(&self, _size: (u32, u32), _point: (u32, u32)) -> Option<&dyn Widget> {None}
// 	fn pick_mut(&mut self, _size: (u32, u32), _point: (u32, u32)) -> Option<&mut dyn Widget> {None}
// }