use gui::widget::Widget;

struct Button {
	message: String,
}

impl Widget for Button {
	fn render(&self, canvas: &mut gui::Canvas, clip: gui::Rect, _bg_color: gui::Color) -> Result<(), String> {
		canvas.set_draw_color(gui::Color::RGB(128, 128, 128));
		canvas.fill_rect(clip)
	}

	gui::pickable!();

	fn event_clicked(&mut self, _size: (u32, u32), _point: (u32, u32)) {
		println!("button clicked: {:?}", self.message);
	}
}

fn main() {
	let app = gui::App::new(
		gui::horizontal![
			Button{message: "Alpha".into()}.margin(10, 10).background(gui::Color::YELLOW),
			gui::widget::Empty.background(gui::Color::CYAN),
		]
	)
		.dimensions(100, 100)
		.fps(30)
	;
	app.run();
	// let app = gui::App::new(gui::horizontal![
	// 	gui::Color::BLUE,
	// 	gui::widget::Margin::new(gui::vertical![
	// 		gui::Color::RED,
	// 		gui::Color::YELLOW,
	// 		Button,
	// 	], 20, 0),
	// 	gui::Color::BLUE,
	// ]);
	// app.run();
}